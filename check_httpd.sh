#!/bin/bash

systemctl status httpd >/dev/null 2>&1

x=`echo $?`
echo $x

if [ "$x" == 0 ]
then
        echo "service online"
elif [ "$x" == 1 ]
then
        echo "Service not available"
elif [ "$x" == 2 ]
then
        echo "Service dead"
elif [ "$x" == 3 ]
then
        echo "Service offline"
else
        echo "Command not found"
fi
