#!/bin/bash

systemctl status jenkins >/dev/null 2>&1

x=`echo $?`

if [ "$x" == 0 ]
then
        echo "service is working fine & online"
        exit 0
elif [ "$x" == 1 ]
then
        echo "Warning- Check System"
        exit 1
elif [ "$x" == 2 ]
then
        echo "Service dead"
elif [ "$x" == 3 ]
then
        echo "Service offline"
        exit 2
else
        echo "Command not found"
fi
